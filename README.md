# Lexicon Classifier using Doc2Vec and K-means #

Nick Glyder, Luke Morrow

## Required Software ##
* Python 3.6+
* PIP Packages:
  * NLTK 
  * SKlearn
  * Gensim

## Sample Run Command ##
python doc2vec_lexicon.py data/cornell data/liu-pos.utf8 data/liu-neg.utf8 \
			--epochs=20 --window=3 --max_alpha=0.025 --lex_filter=True \
			--vector_size=150 --workers=2

## Included Data ##
We have included parsed and prepared data sets for the Cornell and IMDB movie review corpi.
Check the paper + Eisenstein's paper for links and more info.
