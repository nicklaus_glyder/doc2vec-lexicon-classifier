import numpy as np
from scipy import spatial
from sklearn import cluster
from gensim.models.doc2vec import TaggedDocument
from gensim import models

from document import Document, DocType

from sklearn.linear_model import LogisticRegression

def label_from_lexicon_count(words, pos_lex, neg_lex):
    pos_count = 0
    neg_count = 0
    for word in words:
        if word in pos_lex:
            pos_count += 1
        elif word in neg_lex:
            neg_count += 1
    return 'POS' if pos_count >= neg_count else 'NEG'

def k_nearest_neighbor(model, documents, docmap, k=5):
    get_doc = lambda record: docmap[record[0]]

    # Check accuracy
    test_set = list(filter(lambda d: d.doctype & DocType.TEST, documents))

    correct_class = 0.0
    total_class = float(len(test_set))

    for doc in test_set:
        k_nearest = model.docvecs.most_similar(doc.filename, topn=k)
        pos_count = sum(1 for d in k_nearest if get_doc(d).doctype & DocType.POS)
        neg_count = sum(1 for d in k_nearest if get_doc(d).doctype & DocType.NEG)
        classifier = DocType.POS if pos_count >= neg_count else DocType.NEG
        if classifier & doc.doctype:
            correct_class += 1.0

    return correct_class / total_class

def logistic_doc2vec_regression(model, documents):
    vector_size = model.vector_size
    train_set = list(filter(lambda d: d.doctype & DocType.TRAIN, documents))
    train_vectors = np.array([model.docvecs[d.filename] for d in train_set])
    train_labels = np.array([d.label for d in train_set])

    test_set = list(filter(lambda d: d.doctype & DocType.TEST, documents))
    test_vectors = np.array([model.infer_vector(d.tagged_doc.words) for d in test_set])
    test_labels = np.array([d.label for d in test_set])

    classifier = LogisticRegression()
    classifier.fit(train_vectors, train_labels)
    score = classifier.score(test_vectors, test_labels)

    return score

def k_means(model, documents):
    equivalent_labels = False
    train_set = list(filter(lambda d: d.doctype & DocType.TRAIN, documents))
    train_vectors = np.array([model.docvecs[d.filename] for d in train_set])
    train_labels = np.array([d.label for d in train_set])

    test_set = list(filter(lambda d: d.doctype & DocType.TEST, documents))
    test_vectors = np.array([model.infer_vector(d.tagged_doc.words) for d in test_set])
    test_labels = np.array([d.label for d in test_set])

    kmeans = cluster.KMeans(n_clusters=2, random_state=0)
    kmeans.fit(train_vectors)
    labels = kmeans.labels_
    # k means results are non-deterministic so let's figure out which class is which
    if labels[:3].all() == train_labels[:3].all():
        equivalent_labels = True
    predictions = kmeans.predict(test_vectors)
    
    if equivalent_labels:
        accuracy = len([label for label, prediction in zip(test_labels, predictions) if label == prediction]) / len(test_labels)
    else:
        accuracy = len([label for label, prediction in zip(test_labels, predictions) if label != prediction]) / len(test_labels)

    return accuracy